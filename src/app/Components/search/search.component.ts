import { AnimationSequenceMetadata } from '@angular/animations';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import { Annonce } from '../../Class/Annonce/annonce';


@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
  ville: String;
  prixMax: number;
  nbPiece: number;
  myList:any;
  proprieteList:[] = [];
  choixVente:[] = [];
  selectedTypeProprietes:[] = [];
  selectedChoixVente:[] = [];
  choixVenteSettings = {};
  proprieteSettings = {};

  constructor(private http:HttpClient ) { }

  ngOnInit(): void {
    this.proprieteList = [
      { id: 1, text: 'Appartement' },
      { id: 2, text: 'Maison' },
      { id: 3, text: 'Terrain' },
      { id: 4, text: 'Parking' },
    ] as any;
    this.choixVente = [
      { id: 1, text: 'Acheter' },
      { id: 2, text: 'Louer' },
    ] as any;

    this.choixVenteSettings = {
      singleSelection: false,
      idField: 'id',
      textField: 'text',
      selectAllText: 'Les deux',
      unSelectAllText: 'Aucun',
      itemsShowLimit: 2,
      allowSearchFilter: false
    };
    this.proprieteSettings = {
      singleSelection: false,
      idField: 'id',
      textField: 'text',
      selectAllText: 'Tous',
      unSelectAllText: 'Aucun',
      itemsShowLimit: 4,
      allowSearchFilter: false
    };
  }

  findAnnonces() {
    // Choix Vente String : A / AL / L
    const userChoixVente: any = Object.values(this.selectedChoixVente);
    const choixVente = userChoixVente.length == 1 ? userChoixVente[0].text.charAt(0) : userChoixVente.reduce((a: any, b: any) => a.text.charAt(0) + b.text.charAt(0));
    console.log(choixVente);
    
    // Type propriété [String] : [Maison, Appartement]
    const userTypeProprietes: any = Object.values(this.selectedTypeProprietes);
    const typePropriete: [String] = userTypeProprietes.map((obj:any) => obj.text)
    console.log(typePropriete);
    
    // this.http.get<Annonce>(`http://localhost:8080/alten-immo/annonces/annoncesbycriteria/${choixVente}/${typePropriete}/${this.prixMax}/${this.ville}`).
    //   subscribe(response => {
    //     this.myList = response;
    //     // this.message = "articles récupérés";
    //     // this.init();
    //   },
    //   err => {
    //     console.log("KO");
    //   });
  }
}
