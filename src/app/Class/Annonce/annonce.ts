import { Propriete } from "../Propriete/propriete";

export class Annonce {
    titre: String = "monAnnonce";
    description: String = "maDescription";
    noteEnergie: String = "G";
    prix: number = 0;
    etat: boolean = false; // A vendre / Vendue
    propriete: Propriete;
}
