export class Propriete {
    adresse: {
        numero: number;
        rue: String;
        ville: String;
        cp: number;
    } | undefined;
    typeBatiment: String = "Maison"; // Maison - Immeuble - Parking - Terrain
    type: String = "Achat"; // A - AL - L
    prix: number = 0;
    surface: number = 0;
    complements: {
        nbPieces: number,
        etage: number,
        equipements: [String] // [Balcon - Piscine - Garage - cheminée]
    } | undefined;
}
